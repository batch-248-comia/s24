console.log("Hello World!")

let getCube = 5**3;
console.log(`The cube of 5 is ${getCube}!`);

let address = [ 8, "6R", "Cambridge Village", "Cainta", "Philippines"];

let [unit, cluster, village, city, country] = address;
	
console.log(`I live in Unit ${unit}-${cluster} ${village}, ${city}, ${country}.`);


const animal = ["Cassowary", "50kph", "59kg", "6ft", "flightless bird"];

const [name, speed, weight, height, tag] = animal;

console.log(`The ${name} is known to be a ${tag}. It weighs ${weight}, can grow up to ${height} and has a speed of ${speed}!`)

let numbers = ["1", '2', '3', '4', "5"];
numbers.forEach((number)=>{
			console.log(`${number}`)
		})

let reduceNumber = (num1, num2, num3, num4, num5) => num1 + num2 + num3 + num4 + num5

let sum1 = reduceNumber(1,2,3,4,5);
console.log(sum1);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Bex", 10, "Lhasa Apso");
console.log(myDog);
